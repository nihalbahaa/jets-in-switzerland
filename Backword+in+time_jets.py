
# coding: utf-8

# ## split jets to pseudo jets (the same old program).
# ### and get the 4-vector of the pseudo jets as output

# In[3]:

import copy

import threading
from threading import Thread

import math
import random
import numpy as np
import math as m
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import copy
import matplotlib


# In[2]:


# FUNCTIONS
def Theta():                      #define a function to generate random theta 
    ytest = np.log(1+1.5707963267948966)*random.random() #choose theta between 0 and 90
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1 
    return xtest 
def Phi():                      #define a function to generate random theta 
    ytest = np.log(1+6.283185307179586)*random.random() #choose phi between 0 and 360
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1
    return xtest
def Zvalue(): #x-a/b-a=[0,1] where a min of the interval ,and b the max which we want generate value between them
    ytest=(random.random()*(1.0982000000000003))+3.2488  #choose random number between log(25)and log(75)
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest) 
    return int(xtest)   

def glune_diffusion(pInitialStart,pRadStart,Erad,Ecr,f):

    Ef2=Erad/2
    Theta1=Theta()
    Phi1=Phi()
    
    mInitial = [pRadStart[0]-pInitialStart[0] , pRadStart[1]-pInitialStart[1] , pRadStart[2]-pInitialStart[2]]
    
    mRad      = [0,0,0]
    mRad[0]   = Ef2*m.sin(Theta1)*m.cos(Phi1)
    mRad[1]   = Ef2*m.cos(Theta1)*m.sin(Phi1)
    mRad[2]   = Ef2*m.cos(Theta1)
    
    mFinal    = [0,0,0]
    mFinal[0] = mInitial[0]-mRad[0]
    mFinal[1] = mInitial[1]-mRad[1]
    mFinal[2] = mInitial[2]-mRad[2]

    pRad   = [pRadStart[0] + mRad[0],   pRadStart[1] + mRad[1],   pRadStart[2] + mRad[2]]  #calculate the next poin using theta/2
    pFinal = [pRadStart[0] + mFinal[0], pRadStart[1] + mFinal[1], pRadStart[2] + mFinal[2]]    #calculate the point in the opposite direction 

    mRad   = [pRad[0]-pRadStart[0] , pRad[1]-pRadStart[1] , pRad[2]-pRadStart[2]]
    mFinal = [pFinal[0]-pRadStart[0] , pFinal[1]-pRadStart[1] , pFinal[2]-pRadStart[2]]
      
    if Ef2<=Ecr:
        pFinal.insert(0,Ef2)
        f.append(pFinal)
        pRad.insert(0, Ef2)
        f.append(pRad)
    else:
        quark_diffusion(pRadStart, pRad,  Ef2,Ecr,f)
        quark_diffusion(pRadStart, pFinal,Ef2,Ecr,f)


def quark_diffusion(pInitialStart, pFinalStart, Ef, Ecr,f):                  #define a function to make branches 
    
    Erad1 = Ef*(Zvalue()/100)                             #calculate the energy radition using inetial energy and z
    Ef1   = Ef-Erad1 

    Theta1=Theta()
    Phi1=Phi()

    mInitial = [pFinalStart[0]-pInitialStart[0] , pFinalStart[1]-pInitialStart[1] , pFinalStart[2]-pInitialStart[2]]
    
    mRad      = [0,0,0]
    mRad[0]   = Erad1*m.sin(Theta1)*m.cos(Phi1)
    mRad[1]   = Erad1*m.cos(Theta1)*m.sin(Phi1)
    mRad[2]   = Erad1*m.cos(Theta1)
    mFinal    = [0,0,0]
    mFinal[0] = mInitial[0]-mRad[0]
    mFinal[1] = mInitial[1]-mRad[1]
    mFinal[2] = mInitial[2]-mRad[2]
   
    pRad   = [pFinalStart[0] + mRad[0],   pFinalStart[1] + mRad[1],   pFinalStart[2] + mRad[2]]  #calculate the next poin using theta/2
    pFinal = [pFinalStart[0] + mFinal[0], pFinalStart[1] + mFinal[1], pFinalStart[2] + mFinal[2]]    #calculate the point in the opposite direction 
    
    mRad   = [pRad[0]-pFinalStart[0] , pRad[1]-pFinalStart[1] , pRad[2]-pFinalStart[2] ]
    mFinal = [pFinal[0]-pFinalStart[0], pFinal[1]-pFinalStart[1] , pFinal[2]-pFinalStart[2]]
    
    if(Ef1>=Ecr and Erad1>=Ecr):
        Thread(target = glune_diffusion(pFinalStart,pFinal,Erad1,Ecr,f)).start()
        Thread(target = quark_diffusion(pFinalStart,pRad,Ef1,Ecr,f)).start()
        
    elif Ef1>=Ecr:
        quark_diffusion(pFinalStart,pFinal,Ef1,Ecr,f)
   
        pRad.insert(0, Erad1)
        f.append(pRad)
     
    elif Erad1>=Ecr:
        glune_diffusion(pFinalStart,pRad,Erad1,Ecr,f)
        pFinal.insert(0, Ef1)
        f.append(pFinal)
       
    else:
   
        pRad.insert(0, Erad1)
        f.append(pRad)
        pFinal.insert(0, Ef1)        
        f.append(pFinal)
        
def pesudo_jets():
    
    if __name__ == '__main__':
        
        f=[] #final position

        Ecr = 15    
        Ei  = 100

        DirectionInitial=[1,1,2]

        DirectionInitialMag = (DirectionInitial[0]**2 + DirectionInitial[1]**2 + DirectionInitial[2]**2)**0.5

        DirectionInitial[0] = DirectionInitial[0]*(1.0/DirectionInitialMag)
        DirectionInitial[1] = DirectionInitial[1]*(1.0/DirectionInitialMag)
        DirectionInitial[2] = DirectionInitial[2]*(1.0/DirectionInitialMag)

        pInitial    = [0,0,0]
        pInitial[0] = Ei * DirectionInitial[0]
        pInitial[1] = Ei * DirectionInitial[1]
        pInitial[2] = Ei * DirectionInitial[2]
        
        Theta1=Theta()
       
        Phi1=Phi()
        Erad=Ei*(Zvalue() /100)                             #calculate the energy radition using inetial energy and z
        Ef=Ei-Erad  
        mInitial  = pInitial # -[0,0,0]

        mRad      = [0,0,0]
        mRad[0]   = Erad*m.sin(Theta1)*m.cos(Phi1)
        mRad[1]   = Erad*m.cos(Theta1)*m.sin(Phi1)
        mRad[2]   = Erad*m.cos(Theta1)

        mFinal    = [0,0,0]
        mFinal[0] = mInitial[0]-mRad[0]
        mFinal[1] = mInitial[1]-mRad[1]
        mFinal[2] = mInitial[2]-mRad[2]

        pRad   = [pInitial[0] + mRad[0],   pInitial[1] + mRad[1],   pInitial[2] + mRad[2]]  #calculate the next poin using theta/2
        pFinal = [pInitial[0] + mFinal[0], pInitial[1] + mFinal[1], pInitial[2] + mFinal[2]]    #calculate the point in the opposite direction 

        mRad   = [pRad[0]-pInitial[0] , pRad[1]-pInitial[1] , pRad[2]-pInitial[2]]
        mFinal = [pFinal[0]-pInitial[0] , pFinal[1]-pInitial[1] , pFinal[2]-pInitial[2]]

        Thread(target = glune_diffusion(pInitial,pRad,  Erad,Ecr,f)).start()
        Thread(target = quark_diffusion(pInitial,pFinal,Ef,  Ecr,f)).start()
    
    return f  #the 4-vector of the pesudo jets


# ## functions for cluster the jets
# ### the input is the output from split jets program

# In[3]:



def angle (a,b): #function to calculate the angle
    yi=0.5*np.log(-(a[0]+a[3])/(a[0]-a[3]))
    yj=0.5*np.log(-(b[0]+b[3])/(b[0]-b[3]))
    ri=((a[1])**2+(a[2])**2+(a[3])**2)**0.5
    rj=((b[1])**2+(b[2])**2+(b[3])**2)**0.5
    Phi=math.acos(a[3]/ri)
    Phj=math.acos(b[3]/rj)
    delta=(yi-yj)**2+(Phi-Phj)**2
    return delta


def D_iB(f,p): #function to calculate D_iB
    DiB=0
    B=[]#destance of the Beams
    for i in range (len(f)):
        Kt=(f[i][1])**2+(f[i][2])**2
        DiB=Kt**p
        B.append([DiB,i])
    return B


def d_ij(f,p,R): #function to calculate d_ij
   
    kti=0
    ktj=0
    b=[]   #list of the distances between the possibe compination betwwen the particles
    for i in range (len(f)):
        for j in range(i+1,len(f)):
            kti=(f[i][1]**2+f[i][2]**2)**p
            ktj=(f[j][1]**2+f[j][2]**2)**p
            kt=min(kti,ktj)
            d=(kt*angle(f[i],f[j]))/R**2
            b.append([d,i,j])
    return b        


def cluster_process(f,j,p,R): #function to choose the smallest distance and cluster the particle
    v=[]
    b=d_ij(f,p,R)
    B=D_iB(f,p)
    c=b+B
    print ("-------the last of the distance---------")
    for i in range(len(c)):
        v.append(c[i][0])
    print(v)
    print("=============================================================")
    M=min(c)
    m=c.index(M)
    if len(M)==3: #if the B_ij is the smallest distance cluster the particles
        print("the smallest distance is",M[0],"and it is belonge to dij and we will remove the particles","i =",M[1],"j =",M[2])
        print("==============================================================")
        p_new=[f[c[m][1]][0]+f[c[m][2]][0],f[c[m][1]][1]+f[c[m][2]][1],f[c[m][1]][2]+f[c[m][2]][2],f[c[m][1]][3]+f[c[m][2]][3]]#f[bi]+f[bj]
        if c[m][1]>c[m][2]:# this sep to delet the the particle that have bigger index first to not effect on the index of the other
            
            del f[c[m][1]]
            del f[c[m][2]]
        else:
            
            del f[c[m][2]]
            del f[c[m][1]]
        f.append(p_new)
       
        if len(f)>1:
            cluster_process(f,j,p,R)
    else:         #if the B_iB is the smallest distance remove the particle
        print("the smallest distance is",M[0],"and it is belonge to d_iB and we will remove the particle","i =",M[1])
        print("==============================================================")
        j.append(f[c[m][1]])
     
        
        del f[c[m][1]]
    if len(f)>0:    
        cluster_process(f,j,p,R)
    print ("-------------------the last of real jets",j)
    return j


# ## Histogram to see the effect of the p and R perameters

# In[4]:


h1=[]
h2=[]
h3=[]


for i in range(100): #calculate the the numbers of jet with p=1,R=0.05
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,1,0.05)   #(f,j,p,R)
    h1.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=0,R=0.05 
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,0,0.05)
    h2.append(len(j))
    
for i in range(100):  #calculate the the numbers of jet with p=-1,R=0.05
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,-1,0.05)
    h3.append(len(j))

    

    



fig = plt.figure()
ax = plt.subplot(111)




#draw the histogram from large and small R together to the differance 
plt.hist(h1, bins=6,normed=True, color='k', label='p value =1') 
plt.hist(h2, bins=6,normed=True, color='b', alpha=0.5, label='p value =0')
plt.hist(h3, bins=6,normed=True, color='r', alpha=0.5, label='p value =-1')


box = ax.get_position() #legend position
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) #legend position

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
#plt.title("Jets")
plt.xlabel("number of jets for R=0.05")
plt.ylabel("dinesity of the number of jets")
plt.show()    
    
    
    


# In[5]:

h1=[]
h2=[]
h3=[]

for i in range(100):  #calculate the the numbers of jet with p=1,R=0.1
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,1,0.1)
    h1.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=0,R=0.1
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,0,0.1)
    h2.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=-1,R=0.1
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,-1,0.1)
    h3.append(len(j))
    
fig = plt.figure()
ax = plt.subplot(111)


plt.hist(h1, bins=6,normed=True, color='g', label='p value =1') 
plt.hist(h2, bins=6,normed=True, color='y', alpha=0.5, label='p value =0')
plt.hist(h3, bins=6,normed=True, color='r', alpha=0.5, label='p value =-1')

box = ax.get_position() #legend position
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) #legend position

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.title("Jets")
plt.xlabel("number of jets")
plt.ylabel("dinesity of the number of jets")
plt.show()    
        


# In[6]:

h1=[]
h2=[]
h3=[]

for i in range(100):  #calculate the the numbers of jet with p=1,R=0.01
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,1,0.01)
    h1.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=0,R=0.01
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,0,0.01)
    h2.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=-1,R=0.01
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,-1,0.01)
    h3.append(len(j))
    
fig = plt.figure()
ax = plt.subplot(111)


plt.hist(h1, bins=6,normed=True, color='m', label='p value =1') 
plt.hist(h2, bins=6,normed=True, color='g', alpha=0.5, label='p value =0')
plt.hist(h3, bins=6,normed=True, color='y', alpha=0.5, label='p value =-1')

box = ax.get_position() #legend position
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) #legend position

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.title("Jets")
plt.xlabel("number of jets")
plt.ylabel("dinesity of the number of jets")
plt.show()    
        


# In[7]:

h1=[]
h2=[]
h3=[]

for i in range(100): #calculate the the numbers of jet with p=1,R=0.05
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,1,0.5)   #(f,j,p,R)
    h1.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=0,R=0.05
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,0,0.5)
    h2.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=-1,R=0.05
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,-1,0.5)
    h3.append(len(j))
fig = plt.figure()
ax = plt.subplot(111)


plt.hist(h1, bins=6,normed=True, color='c', alpha=0.5, label='p value =1') 
plt.hist(h2, bins=6,normed=True, color='b', alpha=0.5, label='p value =0')
plt.hist(h3, bins=6,normed=True, color='r', alpha=0.5, label='p value =-1')

box = ax.get_position() #legend position
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) #legend position

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.title("Jets")
plt.xlabel("number of jets")
plt.ylabel("dinesity of the number of jets")
plt.show()    
        


# In[8]:

h1=[]
h2=[]
h3=[]

for i in range(100): #calculate the the numbers of jet with p=1,R=1
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,1,1)
    h1.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=0,R=1
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,0,1)
    h2.append(len(j))
    
for i in range(100): #calculate the the numbers of jet with p=-1,R=1
    j=[]
    f=pesudo_jets()
    j=cluster_process(copy.copy(f),j,-1,1)
    h3.append(len(j))  
fig = plt.figure()
ax = plt.subplot(111)


plt.hist(h1, bins=6,normed=True, color='g', alpha=0.5,label='p value =1') 
plt.hist(h2, bins=6,normed=True, color='b', alpha=0.5, label='p value =0')
plt.hist(h3, bins=6,normed=True, color='r', alpha=0.5, label='p value =-1')

box = ax.get_position() #legend position
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) #legend position

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.title("Jets")
plt.xlabel("number of jets")
plt.ylabel("dinesity of the number of jets")
plt.show()    
        


# In[ ]:


    
    


# In[ ]:




# In[ ]:




# In[ ]:



