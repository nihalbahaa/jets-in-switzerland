
# coding: utf-8

# In[4]:

# IMPORTS
import threading
from threading import Thread

import math
import random
import numpy as np
import math as m
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# CONSTANTS


# FUNCTIONS
def Gauss():                      #define a function to generate random theta 
    theta=random.uniform(0, ( math.pi) /6)
    b=random.uniform(0,1)
    x=1/theta
    while b>x:
        Gauss()
    return theta
def Gauss1():                      #define a function to generate random theta 
    theta=random.uniform(0, ( math.pi)/3)
    b=random.uniform(0,1)
    x=1/theta
    while b>x:
        Gauss()
    return theta
def zzz():
    z=(random.randrange(25,75))
    z0=(random.randrange(25,75))
    u=1/z
    while u>z0:
        zzz()
    return z    
def tree0():
    ax.plot([0,3],[0,3],[0,3])
    return 0
def tree1(p1,Erad):
    Ecr=15
    Ef=Erad/2
    p3 =[p1[0]+ Erad*m.sin(Gauss())*m.cos(Gauss1()),p1[1]+ Erad*m.cos(Gauss())*m.sin(Gauss1()),p1[2]+Erad*m.cos(Gauss())]  #calculate the next poin using theta/2
    n=math.asin(Erad*(math.sin(Gauss()))/(Ef))
    p4 =[p1[0]+Ef*m.sin(n)*m.cos(Gauss1()),p1[1]-Ef*m.cos(n)*m.sin(Gauss1()),p1[2]+ Ef*m.cos(n)]    #calculate the point in the opposite direction 
    ax.plot([p1[0],p3[0]],[p1[2],p3[2]],[p1[1],p3[1]])
    ax.plot([p1[0],p4[0]],[p1[2],p4[2]],[p1[1],p4[1]])
    
    if Ef<=Ecr:
        return 0
    else:
        tree2(p3,Ef)
        tree2(p4,Ef)
    #plt.show()  
    return 0 

def tree2(p2,Ef):                  #define a function to make branches 
    Ecr=15
    Erad1=Ef*(zzz()/100)                             #calculate the energy radition using inetial energy and z
    Ef1=Ef-Erad1 
    p5 =[p2[0]+ Erad*m.sin(Gauss())*m.cos(Gauss1()),p2[1]+ Erad*m.cos(Gauss())*m.sin(Gauss1()),p2[2]+Erad*m.cos(Gauss())]  #calculate the next poin using theta/2
    n=math.asin(Erad*(math.sin(Gauss()))/(Ef))
    p6 =[p2[0]+Ef*m.sin(n)*m.cos(Gauss1()),p2[1]-Ef*m.cos(n)*m.sin(Gauss1()),p2[2]+ Ef*m.cos(n)]    #calculate the point in the opposite direction  
    ax.plot([p2[0],p5[0]],[p2[2],p5[2]],[p2[1],p5[1]])
    ax.plot([p2[0],p6[0]],[p2[2],p6[2]],[p2[1],p6[1]])
    if Ef1<=Ecr:
        return 0
    else:
        tree2(p5,Ef1)
    if Erad1<=Ecr:
        return 0
    else:
        tree1(p6,Erad1)
     
    return 0 


if __name__ == '__main__':
   
    Ei=100
    Erad=Ei*(zzz()/100)                             #calculate the energy radition using inetial energy and z
    Ef=Ei-Erad  
    Erad=Ei*(zzz()/100)                             #calculate the energy radition using inetial energy and z
    Ef=Ei-Erad 
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    p1 =[3.0+ Erad*m.sin(Gauss())*m.cos(Gauss1()), 3.0+Erad*m.cos(Gauss())*m.sin(Gauss1()),3.0+Erad*m.cos(Gauss())]  #calculate the next poin using theta/2
    n=math.asin(Erad*(math.sin(Gauss()))/(Ef))
    p2 =[3.0+Ef*m.sin(n)*m.cos(Gauss1()),3.0-Ef*m.cos(n)*m.sin(Gauss1()),3.0+Ef*m.cos(n)]    #calculate the point in the opposite direction 
    ax.plot([3,p1[0]],[3,p1[2]],[3,p1[1]])
    ax.plot([3,p2[0]],[3,p2[2]],[3,p2[1]])
    
    Thread(target = tree0()).start()
    Thread(target = tree1(p1,Erad)).start()
    Thread(target = tree2(p2,Ef)).start()
    

    plt.show()


# In[ ]:



