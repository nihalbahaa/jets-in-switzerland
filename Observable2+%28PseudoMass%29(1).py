
# coding: utf-8

# In[120]:

import copy

import threading
from threading import Thread

import math
import random
import numpy as np
import math as m
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import copy
import matplotlib


# In[2]:

def Theta():                      #define a function to generate random theta 
    ytest = np.log(1+1.5707963267948966)*random.random() #choose theta between 0 and 90
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1 
    return xtest 
def Phi():                      #define a function to generate random theta 
    ytest = np.log(1+6.283185307179586)*random.random() #choose phi between 0 and 360
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1
    return xtest
def Zvalue(): #x-a/b-a=[0,1] where a min of the interval ,and b the max which we want generate value between them
    ytest=(random.random()*(1.0982000000000003))+3.2488  #choose random number between log(25)and log(75)
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest) 
    return int(xtest)   
def glune_diffusion(pInitialStart,pRadStart,Erad,Ecr,f,v):

    Ef2=Erad/2
    Theta1=Theta()
    Phi1=Phi()
    
    mInitial = [pRadStart[0]-pInitialStart[0] , pRadStart[1]-pInitialStart[1] , pRadStart[2]-pInitialStart[2]]
    
    mRad      = [0,0,0]
    mRad[0]   = Ef2*m.sin(Theta1)*m.cos(Phi1)
    mRad[1]   = Ef2*m.cos(Theta1)*m.sin(Phi1)
    mRad[2]   = Ef2*m.cos(Theta1)
    
    mFinal    = [0,0,0]
    mFinal[0] = mInitial[0]-mRad[0]
    mFinal[1] = mInitial[1]-mRad[1]
    mFinal[2] = mInitial[2]-mRad[2]

    pRad   = [pRadStart[0] + mRad[0],   pRadStart[1] + mRad[1],   pRadStart[2] + mRad[2]]  #calculate the next poin using theta/2
    pFinal = [pRadStart[0] + mFinal[0], pRadStart[1] + mFinal[1], pRadStart[2] + mFinal[2]]    #calculate the point in the opposite direction 

    mRad   = [pRad[0]-pRadStart[0] , pRad[1]-pRadStart[1] , pRad[2]-pRadStart[2]]
    mFinal = [pFinal[0]-pRadStart[0] , pFinal[1]-pRadStart[1] , pFinal[2]-pRadStart[2]]
    pFinal.insert(0,Ef2)
    pFinal.append(1)
    f.append(pFinal)
    v.append(pRadStart)
    pRad.insert(0, Ef2)
    pRad.append(0)
    f.append(pRad) 
    v.append(pRadStart)
    return 0
def quark_diffusion(pInitialStart, pFinalStart, Ef, Ecr,f,v):                  #define a function to make branches 
    
    Erad1 = Ef*(Zvalue()/100)                             #calculate the energy radition using inetial energy and z
    Ef1   = Ef-Erad1 

    Theta1=Theta()
    Phi1=Phi()

    mInitial = [pFinalStart[0]-pInitialStart[0] , pFinalStart[1]-pInitialStart[1] , pFinalStart[2]-pInitialStart[2]]
    
    mRad      = [0,0,0]
    mRad[0]   = Erad1*m.sin(Theta1)*m.cos(Phi1)
    mRad[1]   = Erad1*m.cos(Theta1)*m.sin(Phi1)
    mRad[2]   = Erad1*m.cos(Theta1)
    mFinal    = [0,0,0]
    mFinal[0] = mInitial[0]-mRad[0]
    mFinal[1] = mInitial[1]-mRad[1]
    mFinal[2] = mInitial[2]-mRad[2]
   
    pRad   = [pFinalStart[0] + mRad[0],   pFinalStart[1] + mRad[1],   pFinalStart[2] + mRad[2]]  #calculate the next poin using theta/2
    pFinal = [pFinalStart[0] + mFinal[0], pFinalStart[1] + mFinal[1], pFinalStart[2] + mFinal[2]]    #calculate the point in the opposite direction 
    
    mRad   = [pRad[0]-pFinalStart[0] , pRad[1]-pFinalStart[1] , pRad[2]-pFinalStart[2] ]
    mFinal = [pFinal[0]-pFinalStart[0], pFinal[1]-pFinalStart[1] , pFinal[2]-pFinalStart[2]]
    pRad.insert(0, Erad1)
    pRad.append(0)
    f.append(pRad)
    v.append (pFinalStart)
    pFinal.insert(0, Ef1) 
    pFinal.append(1)
    f.append(pFinal)
    v.append(pFinalStart)
    return 0    
def pesudo_jets():
    if __name__ == '__main__':

        f=[] #final position
        v=[] #initial position
        Ecr = 15    
        Ei  = 100

        DirectionInitial=[1,1,2]

        DirectionInitialMag = (DirectionInitial[0]**2 + DirectionInitial[1]**2 + DirectionInitial[2]**2)**0.5

        DirectionInitial[0] = DirectionInitial[0]*(1.0/DirectionInitialMag)
        DirectionInitial[1] = DirectionInitial[1]*(1.0/DirectionInitialMag)
        DirectionInitial[2] = DirectionInitial[2]*(1.0/DirectionInitialMag)

        pInitial    = [0,0,0]
        pInitial[0] = Ei * DirectionInitial[0]
        pInitial[1] = Ei * DirectionInitial[1]
        pInitial[2] = Ei * DirectionInitial[2]
        #print("inital magEnergy = ",(pInitial[0]**2+pInitial[1]**2+pInitial[2]**2)**0.5)    
        Theta1=Theta()

        Phi1=Phi()
        Erad=Ei*(Zvalue() /100)                             #calculate the energy radition using inetial energy and z
        Ef=Ei-Erad  
        mInitial  = pInitial # -[0,0,0]

        mRad      = [0,0,0]
        mRad[0]   = Erad*m.sin(Theta1)*m.cos(Phi1)
        mRad[1]   = Erad*m.cos(Theta1)*m.sin(Phi1)
        mRad[2]   = Erad*m.cos(Theta1)

        mFinal    = [0,0,0]
        mFinal[0] = mInitial[0]-mRad[0]
        mFinal[1] = mInitial[1]-mRad[1]
        mFinal[2] = mInitial[2]-mRad[2]

        pRad   = [pInitial[0] + mRad[0],   pInitial[1] + mRad[1],   pInitial[2] + mRad[2]]  #calculate the next poin using theta/2
        pFinal = [pInitial[0] + mFinal[0], pInitial[1] + mFinal[1], pInitial[2] + mFinal[2]]    #calculate the point in the opposite direction 

        mRad   = [pRad[0]-pInitial[0] , pRad[1]-pInitial[1] , pRad[2]-pInitial[2]]
        mFinal = [pFinal[0]-pInitial[0] , pFinal[1]-pInitial[1] , pFinal[2]-pInitial[2]]
        pRad.insert(0, Erad)
        pRad.append(0)
        f.append(pRad)
        v.append(pInitial)
        pFinal.insert(0, Ef)    
        pFinal.append(1)
        f.append(pFinal)
        v.append(pInitial)
        y=max(f)
        while y[0]>15:

            Y=f.index(y)
            if y[4]==0:
                del f[Y]

                k=[y[1],y[2],y[3]]
                quark_diffusion(v[Y],k,y[0],Ecr,f,v)
                del v[Y]   
            else:
                del f[Y]

                k=[y[1],y[2],y[3]]
                glune_diffusion(v[Y],k,y[0],Ecr,f,v)
                del v[Y]
            y=max(f)
    F=copy.copy(f)
    k=copy.copy(f)
    return f  #the 4-vector of the pesudo jets  


# In[76]:

def angle (a,b): #function to calculate the angle
    yi=0.5*np.log(-(a[0]+a[3])/(a[0]-a[3]))
    yj=0.5*np.log(-(b[0]+b[3])/(b[0]-b[3]))
    ri=((a[1])**2+(a[2])**2+(a[3])**2)**0.5
    rj=((b[1])**2+(b[2])**2+(b[3])**2)**0.5
    Phi=math.acos(a[3]/ri)
    Phj=math.acos(b[3]/rj)
    delta=(yi-yj)**2+(Phi-Phj)**2
    return delta


def D_iB(f,p): #function to calculate D_iB
    DiB=0
    B=[]#destance of the Beams
    for i in range (len(f)):
        Kt=(f[i][1])**2+(f[i][2])**2
        DiB=Kt**p
        B.append([DiB,i])
    return B


def d_ij(f,p,R): #function to calculate d_ij
   
    kti=0
    ktj=0
    b=[]   #list of the distances between the possibe compination betwwen the particles
    for i in range (len(f)):
        for j in range(i+1,len(f)):
            kti=(f[i][1]**2+f[i][2]**2)**p
            ktj=(f[j][1]**2+f[j][2]**2)**p
            kt=min(kti,ktj)
            d=(kt*angle(f[i],f[j]))/R**2
            b.append([d,i,j])
    return b        


def cluster_process(f,j,p,R): #function to choose the smallest distance and cluster the particle
    v=[]
    b=d_ij(f,p,R)
    B=D_iB(f,p)
    c=b+B
    #print ("-------the last of the distance---------")
    for i in range(len(c)):
        v.append(c[i][0])
    #print(v)
    #print("=============================================================")
    M=min(c)
    m=c.index(M)
    if len(M)==3: #if the B_ij is the smallest distance cluster the particles
        #print("the smallest distance is",M[0],"and it is belonge to dij and we will remove the particles","i =",M[1],"j =",M[2])
        #print("==============================================================")
        p_new=[f[c[m][1]][0]+f[c[m][2]][0],f[c[m][1]][1]+f[c[m][2]][1],f[c[m][1]][2]+f[c[m][2]][2],f[c[m][1]][3]+f[c[m][2]][3]]#f[bi]+f[bj]
        if c[m][1]>c[m][2]:# this sep to delet the the particle that have bigger index first to not effect on the index of the other
            
            del f[c[m][1]]
            del f[c[m][2]]
        else:
            
            del f[c[m][2]]
            del f[c[m][1]]
        f.append(p_new)
       
        if len(f)>1:
            cluster_process(f,j,p,R)
    else:         #if the B_iB is the smallest distance remove the particle
        #print("the smallest distance is",M[0],"and it is belonge to d_iB and we will remove the particle","i =",M[1])
        #print("==============================================================")
        j.append(f[c[m][1]])
     
        
        del f[c[m][1]]
    if len(f)>0:    
        cluster_process(f,j,p,R)
    #print ("-------------------the last of real jets",j)
    return j


# In[78]:

def cluster(f,j,k,p,R,O): #function to choose the smallest distance and cluster the particle
    v=[]
    #print(f)
    b=d_ij(f,p,R)
    B=D_iB(f,p)
    c=b+B
    #print ("-------the last of the distance---------")
    for i in range(len(c)):
        v.append(c[i][0])
    #print(v)
    #print("=============================================================")
    M=min(c)
    m=c.index(M)
    if len(M)==3: #if the B_ij is the smallest distance cluster the particles
        #print("the smallest distance is",M[0],"and it is belonge to dij and we will remove the particles","i =",M[1],"j =",M[2])
        #print("==============================================================")
        p_new=[f[c[m][1]][0]+f[c[m][2]][0],f[c[m][1]][1]+f[c[m][2]][1],f[c[m][1]][2]+f[c[m][2]][2],f[c[m][1]][3]+f[c[m][2]][3]]#f[bi]+f[bj]
        
        if k==f[c[m][1]][0]+f[c[m][2]][0]:
            
            O.append(f[M[1]])
            O.append(f[M[2]])
            #cluster(f,j,k,p,R,O)
            
        if c[m][1]>c[m][2]:# this sep to delet the the particle that have bigger index first to not effect on the index of the other
            
            del f[c[m][1]]
            del f[c[m][2]]
            
        else:
            
            del f[c[m][2]]
            del f[c[m][1]]
        f.append(p_new)
       
        if len(f)>1:
            cluster(f,j,k,p,R,O)
    else:         #if the B_iB is the smallest distance remove the particle
        #print("the smallest distance is",M[0],"and it is belonge to d_iB and we will remove the particle","i =",M[1])
        #print("==============================================================")
        j.append(f[c[m][1]])
     
        
        del f[c[m][1]]
    if len(f)>0:    
        cluster(f,j,k,p,R,O)
    #print ("-------------------the last of real jets",j)
    return O


# In[79]:

def PseudoMass(p,R):
    
    j=[]
    f=pesudo_jets()
    #print(f)
    F=copy.copy(f)
    #print(f)
    J=cluster_process(f,j,p,R)
    #print(j)
    L=max(J)
    if L in F:  #this case when the pseudojet itself is the jet
        s=L
        PMass=-1
    else:       #when more than one pseudojet cluster together to give the jet 
        j=[]
        D=copy.copy(F)
        O=[] 

        s=cluster(F,j,L[0],p,R,O)
        S=max(s)
        SS=s.index(S)
        #print(s)
        while S[0]>15:
            j=[]
            O=[]

            A=cluster(copy.copy(D),j,S[0],p,R,O)

            del s[SS]
            s.append(A[0])
            s.append(A[1])
            #print(s)
            S=max(s)
            SS=s.index(S)
        MAX=[]
        S=max(s)
        MAX.append(S)
        SS=s.index(S)
        del s[SS]
        S=max(s)
        MAX.append(S)
        #print(MAX)
        G=d_ij(MAX,p,R)
        PMass=MAX[0][0]*MAX[1][0]*G[0][0]
    
    return PMass


# In[80]:

PseudoMass(0,1)


# In[82]:

def filterNoneType(lis):
    lis2 = []
    for l in range(len(lis)): #filter out NoneType
        if lis[l] !=str:
        #if if(isinstance(lis[l],float)):
            lis2.append(lis[l])
    #print(lis2)
    return lis2


# In[83]:

get_ipython().magic('pinfo plt.hist')


# In[113]:

def plots(R,T):
    h1=[]
    h2=[]
    h3=[]


    for i in range(100): #calculate the the numbers of jet with p=1,R=0.05
        h1.append(PseudoMass(1,R) )
    #H1= filterNoneType(h1)
    b1=int(max(h1)-min(h1)) #calculate numbers of bins
    h1=np.nan_to_num(h1)
    print(h1)
    for i in range(100): #calculate the the numbers of jet with p=0,R=0.05 
        h2.append(PseudoMass(0,R) )
    #H2= filterNoneType(h2)
        #H2 =[x for x in h2 if (x != 'NaN' and x!='None') ] 
    b2=int(max(h2)-min(h2)) #calculate numbers of bins
    print(h2)
    h2=np.nan_to_num(h2)
    for i in range(100):  #calculate the the numbers of jet with p=-1,R=0.05
        h3.append(PseudoMass(-1,R) )
    #H3= filterNoneType(h3)
       
    b3=int(max(h3)-min(h3)) #calculate numbers of bins
    h3=np.nan_to_num(h3)
    print(h3)




    fig = plt.figure()
    ax = plt.subplot(111)



    if R==1:
        b1=b2=b3=10
        
    #draw the histogram from large and small R together to the differance 
    plt.hist(h1, bins=15, color='r',alpha=0.5 ,label='p value =1') 
    plt.hist(h2, bins=15, color='b',alpha=0.5 ,label='p value =0')
    plt.hist(h3, bins=15, color='g',alpha=0.5 ,label='p value =-1')


    box = ax.get_position() #legend position
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) #legend position

    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    #plt.title("Jets")
    plt.xlabel(T)
    plt.ylabel("dinesity of PseudoMass of the highest jet")
    plt.show()    

    
if __name__ == '__main__':
    
    plots(0.05,"PseudoMass R=0.05")
    plots(0.01,"PseudoMass R=0.01")
    plots(0.1,"PseudoMass R=0.1")
    plots(0.5,"PseudoMass R=0.5")
    plots(1,"PseudoMassR=1")
 


# In[111]:

h2=[]
for i in range(100): #calculate the the numbers of jet with p=0,R=0.05 
        h2.append(PseudoMass(0,0.05) )
    #H2= filterNoneType(h2)
        #H2 =[x for x in h2 if (x != 'NaN' and x!='None') ] 
    #b2=int(max(h2)-min(h2)) #calculate numbers of bins
    #print(h2)
h2=np.nan_to_num(h2)

plt.hist(h2, bins=15, color='b', label='p value =0')
plt.show()


# In[121]:

def plots(p,T):
    h1=[]
    h2=[]
    h3=[]
    h4=[]
    h5=[]
    for i in range(100): #calculate the the numbers of jet with p=1,R=0.05
        h1.append(PseudoMass(p,0.05))
        #H1= filterNoneType(h1)
        #b1=int(max(h1)-min(h1)) #calculate numbers of bins
        h1=np.nan_to_num(h1)
    print(h1)

    for i in range(100): #calculate the the numbers of jet with p=1,R=0.01
        
        h2.append(PseudoMass(p,0.01))
        #H2= filterNoneType(h2)
        #b2=int(max(h1)-min(h2)) #calculate numbers of bins
        h2=np.nan_to_num(h2)
    print(h2)


    for i in range(100): #calculate the the numbers of jet with p=1,R=0.01
        
        h1.append(PseudoMass(p,0.1))
        #H3= filterNoneType(h1)
        #b3=int(max(h3)-min(h3)) #calculate numbers of bins
        h3=np.nan_to_num(h3)
    print(h3)


    for i in range(100): #calculate the the numbers of jet with p=1,R=0.01
        h4.append(PseudoMass(p,0.5))
        #H4= filterNoneType(h4)
        #b4=int(max(h4)-min(h4)) #calculate numbers of bins
        h4=np.nan_to_num(h4)
    print(h4)


    for i in range(100): #calculate the the numbers of jet with p=1,R=0.01
        h5.append(PseudoMass(p,1))
        #H5= filterNoneType(h5)
        #b5=int(max(h5)-min(h5)) #calculate numbers of bins
        h5=np.nan_to_num(h5)
    print(h5)   


    fig = plt.figure()
    ax = plt.subplot(111)


    plt.hist(h1, bins=15, color='r',normed=True,histtype='step', label='R =0.05') 
    plt.hist(h2, bins=15, color='b',normed=True, histtype='step', label='R =0.01')
    plt.hist(h3, bins=15, color='g',normed=True,histtype='step', label='R =0.1')
    plt.hist(h4, bins=15, color='y',normed=True,histtype='step', label='R =0.5') 
    plt.hist(h5, bins=15, color='m',normed=True, histtype='step', label='R =1')

    box = ax.get_position() #legend position
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height]) #legend position

    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    #plt.title("Jets")
    plt.xlabel(T)
    plt.ylabel("dinesity of the Energy of jets")
    plt.show()    
plots(1,"Energy of jets when p= 1")
plots(0,"Energy of jets when p= 0")
plots(-1,"Energy of jets when p= -1")


# In[ ]:



