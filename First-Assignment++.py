
# calculating values of py

import random
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
from mpmath import *



def withinCircle(x,y):      #define afunction to decide witch points it's in the circle
    if(x**2+y**2<1):
        return True
    else:
        return False
a=[]
for k in range(0,100):
    
    

    circleArea = 0
    squareArea = 0
    pi = 0
    for i in range(0,10000):
    
        x = random.random()                  #generate random point
        y = random.random()
     
        if(withinCircle(x,y)==1):            #check if this point inside the circle
        
            circleArea=circleArea+1
        squareArea=squareArea+1
    pi = 4.0*circleArea/squareArea
    a.append(pi)

# plot a histogram of pi  value


plt.hist(a,bins=6,normed=True)
plt.title("value of pi")
plt.plot()
plt.show()

## generate theta
#### using propability 

c=[]
for l in range(1,1000):
    a=(random.uniform(0,  math.pi /4)*180 )/math.pi   #generate random angle
    b=(random.uniform(0,  math.pi /4)*180 )/math.pi   #generate another random angle
    x=(math.exp(-a**2/2)/math.sqrt(2*math.pi))*100    #calculate the propability of point a
    if b<x :                                          #compare if the second random number is less than the probapility of the first number  
        c.append(a)

## generate theta
#### using integration monto carlo             

#the same as the above but integrated the probability 


mp.dps =50
c=[]
for l in range(1,100):
    a=random.uniform(0,  math.pi /2)
    b=random.uniform(0,1)
    x=1/2*(erf(a/math.sqrt(2)))
    if b<x :
        c.append(a)


## histogram



plt.hist(c,bins=12,normed=True)

plt.title("value of pi")
plt.plot()
plt.show()

# Monte Carlo simulation 



plt.plot([0,1],[0,0])             #drow the first arrow
x=[]
y=[]
def Gauss():                      #define a function to generate random theta 
    theta1=random.uniform(0,  math.pi /2)
    b1=random.uniform(0,1)
    x1=1/2*(erf(theta1/math.sqrt(2)))
    return theta1,b1,x1

def tree(p0,Ei):                  #define a function to make branches 
    N=0
    N+=1
    theta=random.uniform(0,  math.pi /2)
    b=random.uniform(0,1)
    x=1/2*(erf(theta/math.sqrt(2)))
    while b>x:
        theta,b,x=Gauss()      #call the function theta to generate random theta
    #print (theta)
    p1 =[p0[0]+ 1.0, p0[1] +  math.tan(theta/2)]    #calculate the next poin using theta/2
    p2 =[p0[0]+ 1.0, p0[1] - math.tan(theta/2)]    #calculate the point in the opposite direction 
    
    plt.plot([p0[0],p1[0]],[p0[1],p1[1]])       #plot the new arrows 
    plt.plot([p0[0],p2[0]],[p0[1],p2[1]])
    
    Ecr=24                                      #define the critical energy that the particle will stop split after it
    
    if N %2==0:                               #just use it to simulate the split of the gloun to two quarks have the same energy
        Ef=Ei/2                                
        Erad=Ei/2                            
    else:                                     #just use it to simulate the split of the quark to one quark and one glune with energy Ef and Erad
        z=1/((random.randrange(25,75))/100)   #generate 1/z randomly using the value z
        Erad=Ei/z                             #calculate the energy radition using inetial energy and z
        Ef=Ei-Erad                            #calculate the final energy of the quark
    if Ef<=Ecr:                                #check if Ef less than the critical energy to decide if it will split again 
        return 0
    else:                                     #othewise split to two arrowa with random angle betwwen them
        tree(p1,Ef)                           #call the function tree
        #tree(p2,Ef)
        
        #Ei=Ef
        
    if Erad<=Ecr:                           #check if the energy radiation less than tha critical energy
        return 0
    else:
        #tree(p1,Erad)
        tree(p2,Erad)
        
        #Ei=Erad/2
        
    return 0 
    
tree([1,0],100)                           #call the function tree with initial poin[1,0]and initial energy of 100
#plt.plot(x1,y1,'-or')
plt.show()                                #show the plot

## todo
# CONVERT ALL OF ABOVE TO FUNCTION DEFINITIONS ONLY
if __name__ == "__main__":
	print("hello world")

