
# coding: utf-8

# In[19]:

# IMPORTS
import threading
from threading import Thread

import math
import random
import numpy as np
import math as m
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D




# FUNCTIONS
def Theta():                      #define a function to generate random theta 
    ytest = np.log(1+0.5235987755982988)*random.random()
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1 
    return xtest 
def Phi():                      #define a function to generate random theta 
    ytest = np.log(1+1.0471975511965976)*random.random()
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1
    return xtest
def Zvalue(): #x-a/b-a=[0,1] where a min of the interval ,and b the max which we want generate value between then
    ytest=(random.random()*(1.0982000000000003))+3.2488
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest) 
    return int(xtest)   
def initail_line():
    ax.plot([0,3],[0,3],[0,3])
    return 0
def glune_diffusion(p1,Erad,Ecr):
    Ef=Erad/2
    p3 =[p1[0]+ Erad*m.sin(Theta())*m.cos(Phi()),p1[1]+ Erad*m.cos(Theta())*m.sin(Phi()),p1[2]+Erad*m.cos(Theta())]  #calculate the next poin using theta/2
    n=math.asin(Erad*(math.sin(Theta()))/(Ef))
    p4 =[p1[0]+Ef*m.sin(n)*m.cos(Phi()),p1[1]-Ef*m.cos(n)*m.sin(Phi()),p1[2]+ Ef*m.cos(n)]    #calculate the point in the opposite direction 
    ax.plot([p1[0],p3[0]],[p1[2],p3[2]],[p1[1],p3[1]])
    ax.plot([p1[0],p4[0]],[p1[2],p4[2]],[p1[1],p4[1]])
    
    if Ef<=Ecr:
        return 0
    else:
        quark_diffusion(p3,Ef,Ecr)
        quark_diffusion(p4,Ef,Ecr)
    #plt.show()  
    return 0 

def quark_diffusion(p2,Ef,Ecr):                  #define a function to make branches 
    Erad1=Ef*(Zvalue()/100)                             #calculate the energy radition using inetial energy and z
    Ef1=Ef-Erad1 
    p5 =[p2[0]+ Erad*m.sin(Theta())*m.cos(Phi()),p2[1]+ Erad*m.cos(Theta())*m.sin(Phi()),p2[2]+Erad*m.cos(Theta())]  #calculate the next poin using theta/2
    n=math.asin(Erad*(math.sin(Theta()))/(Ef))
    p6 =[p2[0]+Ef*m.sin(n)*m.cos(Phi()),p2[1]-Ef*m.cos(n)*m.sin(Phi()),p2[2]+ Ef*m.cos(n)]    #calculate the point in the opposite direction  
    ax.plot([p2[0],p5[0]],[p2[2],p5[2]],[p2[1],p5[1]])
    ax.plot([p2[0],p6[0]],[p2[2],p6[2]],[p2[1],p6[1]])
    if Ef1<=Ecr:
        return 0
    else:
        quark_diffusion(p5,Ef1,Ecr)
    if Erad1<=Ecr:
        return 0
    else:
        glune_diffusion(p6,Erad1,Ecr)
     
    return 0 

if __name__ == '__main__':
    Ecr=15
    Ei=100
    Erad=Ei*(Zvalue() /100)                             #calculate the energy radition using inetial energy and z
    Ef=Ei-Erad  
    Erad=Ei*(Zvalue() /100)                             #calculate the energy radition using inetial energy and z
    Ef=Ei-Erad 
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    p1 =[3.0+ Erad*m.sin(Theta())*m.cos(Phi()), 3.0+Erad*m.cos(Theta())*m.sin(Phi()),3.0+Erad*m.cos(Theta())]  #calculate the next poin using theta/2
    n=math.asin(Erad*(math.sin(Theta()))/(Ef))
    p2 =[3.0+Ef*m.sin(n)*m.cos(Phi()),3.0-Ef*m.cos(n)*m.sin(Phi()),3.0+Ef*m.cos(n)]    #calculate the point in the opposite direction 
    ax.plot([3,p1[0]],[3,p1[2]],[3,p1[1]])
    ax.plot([3,p2[0]],[3,p2[2]],[3,p2[1]])
    
    Thread(target = initail_line()).start()
    Thread(target = glune_diffusion(p1,Erad,Ecr)).start()
    Thread(target = quark_diffusion(p2,Ef,Ecr)).start()
    

    plt.show()


# In[ ]:



